
![ban](Media/img/ban.png)
# Lipidium

Su & Mo are two sumotori training hard to win the next world sumo championship. The gods, however, are disgusted by their fat acquisition regime and decided to teach them a lesson. They are also pretty bored and chose to connect them with an umbilical cord allowing them to transfer fat to each other. Su and Mo refusing to give up on their dream train to exploit this new ability.


____

**Jam year**: 2018

**Platforms**:
MS Windows, Linux / Unix, Web standard (HTML5, Java, JavaScript, Flash), Android device

**Tools and Technologies**:
Unreal Engine

**Technology Notes**:
Graphisms were created using Blender. Gamedesign used paper & pen and a whiteboard.

**Credits**: Luc Clavel, Thibault Harlé, Laure Le Sidaner, Swann Martinez, Mathieu Rousse
